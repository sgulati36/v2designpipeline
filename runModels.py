#!/usr/bin/env python

import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import MinMaxScaler
from sklearn.svm import SVR, SVC
from sklearn.metrics import r2_score, balanced_accuracy_score, accuracy_score
from sklearn.neighbors import KNeighborsRegressor, KNeighborsClassifier
from sklearn.model_selection import train_test_split

inFile = sys.argv[1]
outFileBase = sys.argv[2]

def splitFunction(a,b,size):
	newX_Train = pd.DataFrame()
	newY_Train = []
	newX_Test = pd.DataFrame()
	newY_Test = []
	for i in b.value_counts().index:
		x_train, x_test, y_train, y_test = train_test_split(a[b==i], b[b==i], test_size=size)
		newX_Train = newX_Train.append(x_train)
		newX_Test = newX_Test.append(x_test)
		newY_Train.extend(y_train.tolist())
		newY_Test.extend(y_test.tolist())
	return(newX_Train,newX_Test,newY_Train,newY_Test)

df = pd.read_csv(inFile,sep="\t",index_col=0)

renamer = {'fwd_GC_perc':'fwd_GC','rev_GC_perc':'rev_GC','amplicon_len':'amplicon_length','fwd_length':'fwd_len','rev_length':'rev_len','fwd_primer_maxN':'fwd_maxN','rev_primer_maxN':'rev_maxN','amp_GC':'ampliconGC'}

df = df.rename(index=str,columns=renamer)

y = df['NormalizedMean']
x = df[['fwd_GC','fwd_Tm','rev_GC','rev_Tm','amplicon_length','fwd_len','rev_len','ampliconGC','fwd_num_hits_no_mismatch_in_last5','rev_num_hits_no_mismatch_in_last5']]

yC = y.copy().astype(str)
x2 = pd.DataFrame()
y2 = []
ySmall = []

for i in range(len(y)):
	if y[i] < 0.2:
		yC[i] = 'low'
		x2 = x2.append(x.iloc[i])
		y2.append(yC[i])
		ySmall.append(y[i])
	elif y[i] >= 0.2 and y[i] <=2:
		yC[i] = 'ok'
		x2 = x2.append(x.iloc[i])
		y2.append(yC[i])
		ySmall.append(y[i])
	else:
		yC[i] = 'high'

y2 = pd.Series(data=y2,index=x2.index) 
ySmall = pd.Series(data=ySmall)
yCmasked = yC.copy()
yCmasked[yC == "high"] = "low"

accuracies1 = []
accuracies2 = []
balancedAccuracies1 = []
balancedAccuracies2 = []
properties = [x,x,x2]
classes = [yC,yCmasked,y2]
dataType = ['Original Data', 'Masked Data', 'Reduced Data']

for j in range(len(properties)):
	temp1 = []
	temp2 = []
	temp1Balanced = []
	temp2Balanced = []
	for i in range(10000):
		x_train, x_test, y_train, y_test = splitFunction(properties[j], classes[j],0.3)
		model1 = KNeighborsClassifier(weights='distance',algorithm='brute')
		model1.fit(x_train,y_train)
		model2 = SVC(kernel='rbf',gamma='auto')
		model2.fit(x_train,y_train)
		temp2.append(accuracy_score(y_test,model2.predict(x_test)))
		temp1.append(accuracy_score(y_test,model1.predict(x_test)))
		temp1Balanced.append(balanced_accuracy_score(y_test,model1.predict(x_test)))
		temp2Balanced.append(balanced_accuracy_score(y_test,model2.predict(x_test)))

	accuracies1.append(temp1)
	accuracies2.append(temp2)
	balancedAccuracies1.append(temp1Balanced)
	balancedAccuracies2.append(temp2Balanced)

fig, axs = plt.subplots(3, 2, figsize=(10, 10), sharey='row')
sns.distplot(accuracies1[0],ax = axs[0][0]).set_title('KNC Original Data')
axs[0][0].axvline(np.mean(accuracies1[0]),color='red')
axs[0][0].axvline(np.median(accuracies1[0]),color='green')
sns.distplot(accuracies2[0],ax = axs[0][1]).set_title('SVC Original Data')
axs[0][1].axvline(np.mean(accuracies2[0]),color='red')
axs[0][1].axvline(np.median(accuracies2[0]),color='green')
sns.distplot(accuracies1[1],ax = axs[1][0]).set_title('KNC Masked Data')
axs[1][0].axvline(np.mean(accuracies1[1]),color='red')
axs[1][0].axvline(np.median(accuracies1[1]),color='green')
sns.distplot(accuracies2[1],ax = axs[1][1]).set_title('SVC Masked Data')
axs[1][1].axvline(np.mean(accuracies2[1]),color='red')
axs[1][1].axvline(np.median(accuracies2[1]),color='green')
sns.distplot(accuracies1[2],ax = axs[2][0]).set_title('KNC Reduced Data')
axs[2][0].axvline(np.mean(accuracies1[2]),color='red')
axs[2][0].axvline(np.median(accuracies1[2]),color='green')
sns.distplot(accuracies2[2],ax = axs[2][1]).set_title('SVC Reduced Data')
axs[2][1].axvline(np.mean(accuracies2[2]),color='red')
axs[2][1].axvline(np.median(accuracies2[2]),color='green')
plt.savefig(outFileBase+"_accuracyScore.png")
plt.clf()

fig, axs = plt.subplots(3, 2, figsize=(10, 10), sharey='row')
sns.distplot(balancedAccuracies1[0],ax = axs[0][0]).set_title('KNC Original Data')
axs[0][0].axvline(np.mean(balancedAccuracies1[0]),color='red')
axs[0][0].axvline(np.median(balancedAccuracies1[0]),color='green')
sns.distplot(balancedAccuracies2[0],ax = axs[0][1]).set_title('SVC Original Data')
axs[0][1].axvline(np.mean(balancedAccuracies2[0]),color='red')
axs[0][1].axvline(np.median(balancedAccuracies2[0]),color='green')
sns.distplot(balancedAccuracies1[1],ax = axs[1][0]).set_title('KNC Masked Data')
axs[1][0].axvline(np.mean(balancedAccuracies1[1]),color='red')
axs[1][0].axvline(np.median(balancedAccuracies1[1]),color='green')
sns.distplot(balancedAccuracies2[1],ax = axs[1][1]).set_title('SVC Masked Data')
axs[1][1].axvline(np.mean(balancedAccuracies2[1]),color='red')
axs[1][1].axvline(np.median(balancedAccuracies2[1]),color='green')
sns.distplot(balancedAccuracies1[2],ax = axs[2][0]).set_title('KNC Reduced Data')
axs[2][0].axvline(np.mean(balancedAccuracies1[2]),color='red')
axs[2][0].axvline(np.median(balancedAccuracies1[2]),color='green')
sns.distplot(balancedAccuracies2[2],ax = axs[2][1]).set_title('SVC Reduced Data')
axs[2][1].axvline(np.mean(balancedAccuracies2[2]),color='red')
axs[2][1].axvline(np.median(balancedAccuracies2[2]),color='green')
plt.savefig(outFileBase+"_balancedAccuracyScore.png")
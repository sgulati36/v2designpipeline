#!/usr/bin/env python

# Use python3

# Usage ./metrics.py tsvList <OUTPUT FILENAME>

# To generate tsvList run `ls */*.cell.distribution.tsv > tsvList` in /data/V2_DesignPipeline/tmp_DNAnexus_runs 

import pandas as pd
import numpy as np
import sys,os

with open(sys.argv[1]) as INPUT:
	fileList = INPUT.read().strip().split("\n")
	
metrics = []
tempMetrics = []
tempMetrics.append('run name')
tempMetrics.append('tube number')
tempMetrics.append('10x 60% PCA filter')
tempMetrics.append('panelSize')
tempMetrics.append('number of Cells')
tempMetrics.append('percent reads mapped to target')
tempMetrics.append('percent reads mapped to cells')
tempMetrics.append('panel uniformity >=0.2x')
tempMetrics.append('40x coverage')
tempMetrics.append('amplicons between 0.5x and 2x')
tempMetrics.append('amplicons between 0.2x and 5x')
tempMetrics.append('percent reads to greater 2x mean amplicons')
tempMetrics.append('amplicons has 20x per cell')
tempMetrics.append('amplicons has 10x per cell')
tempMetrics.append('amplicons has 5x per cell')
tempMetrics.append('amplicons has 1x per cell')
tempMetrics.append('total reads')
tempMetrics.append('threshold 0.2x mean')
tempMetrics.append('cell uniformity before dropping')
tempMetrics.append('cell uniformity after dropping at 0.8')
tempMetrics.append('cell uniformity after dropping at 0.9')
tempMetrics.append('cell uniformity after dropping at 1')
tempMetrics.append('droppedAmplicons')
tempMetrics = list(map(str,tempMetrics))
metrics.append("\t".join(tempMetrics))
	
for inFile in fileList:
	parentDir = inFile.split("/")[0]
	allFiles = os.listdir(parentDir)
	searchString = inFile.split("/")[1].replace(".barcode.cell.distribution.tsv","")
	for file in allFiles:
		if "summary" in file:
			summaryFile = parentDir + "/" + file
	try:
		[runName,tubeNumber] = parentDir.split("-")
		if "_10X60FILTEROFF" in tubeNumber:
			tubeNumber = tubeNumber.split("_")[0]
			filterStatus = "Off"
		else:
			filterStatus = "On"
	except:
		if "_10X60FILTEROFF" in parentDir:
			parentDir = parentDir.replace("_10X60FILTEROFF","")
			filterStatus = "Off"
		else:
			filterStatus = "On"
		[runName,tubeNumber] = [parentDir,"1"]

	data = {}

	with open(inFile) as INPUT:
		headers = INPUT.readline().strip().split("\t")
		if headers[0] == "cell" or headers[0] == "cell_barcode": # this line has to be changed based on input file
			headers = headers[1:]
		for line in INPUT:
			temp = line.strip().split("\t")
			if temp[0] in data:
				print("Repeat barcode", temp[0])
			else:
				data[temp[0]] = list(map(int,temp[1:]))

	df = pd.DataFrame.from_dict(data, orient="index",columns=headers)
	#outFile = inFile.replace('.barcode.cell.distribution.tsv','') + ".ampAvg.txt"
	#outFile2 = inFile.replace('.barcode.cell.distribution.tsv','') + ".ampAvgNoNormal.txt"
	outFile = "/".join(inFile.split("/")[:-1]) + "/"  + runName + "-" + tubeNumber + ".ampAvg.txt"
	outFile2 = "/".join(inFile.split("/")[:-1]) + "/"  + runName + "-" + tubeNumber + ".ampAvgNoNormal.txt"

	totalReads = df.sum().sum()
	panelSize = len(headers)
	numCells = len(data)
	meanReads = df.mean().mean()
	ampliconAverages = df.mean()
	panelUniformity = (ampliconAverages.ge(0.2 * meanReads).sum() / panelSize) * 100
	badAmplicons = np.asarray(headers)[ampliconAverages.lt(0.2 * meanReads)]
	fourtyCov = (ampliconAverages.ge(40).sum() / panelSize) * 100
	betweenPoint5And2 = (ampliconAverages.between(left = (0.5 * meanReads), right = (2 * meanReads), inclusive= True).sum()/panelSize) * 100
	betweenPoint2And5 = (ampliconAverages.between(left = (0.2 * meanReads), right = (5 * meanReads), inclusive= True).sum()/panelSize) * 100
	twentyXamplicons = (ampliconAverages.ge(20).sum() / panelSize ) * 100
	tenXamplicons = (ampliconAverages.ge(10).sum() / panelSize ) * 100
	fiveXamplicons = (ampliconAverages.ge(5).sum() / panelSize ) * 100
	oneXamplicons = (ampliconAverages.ge(1).sum() / panelSize ) * 100
	highFlyerAmplicons = np.asarray(headers)[ampliconAverages.ge(2 * meanReads)].tolist()
	percentReadsToHighFlyers = (df[highFlyerAmplicons].sum().sum() / totalReads) * 100
	
	normalizedValues = ampliconAverages/ampliconAverages.mean()
	normalizedValues.to_csv(outFile,sep="\t")
	ampliconAverages.to_csv(outFile2,sep="\t")
	badAmpliconsDropped = df.drop(columns = badAmplicons)
	cellUniformity1 = ((df.ge(meanReads * 0.2).sum(axis=1).apply(lambda x: x/len(list(df))).ge(0.8).sum()) / numCells ) * 100
	cellUniformity2 = ((badAmpliconsDropped.ge(meanReads * 0.2).sum(axis=1).apply(lambda x: x/len(list(badAmpliconsDropped))).ge(0.8).sum()) / numCells ) * 100
	cellUniformity3 = ((badAmpliconsDropped.ge(meanReads * 0.2).sum(axis=1).apply(lambda x: x/len(list(badAmpliconsDropped))).ge(0.9).sum()) / numCells ) * 100
	cellUniformity4 = ((badAmpliconsDropped.ge(meanReads * 0.2).sum(axis=1).apply(lambda x: x/len(list(badAmpliconsDropped))).ge(1).sum()) / numCells ) * 100
	numAmpliconsDropped = len(badAmplicons)

	with open(summaryFile) as INPUT:
		data = INPUT.read().strip().split("\n")
	
	for line in data:
		temp = line.split("\t")
		if searchString in line:
			if len(temp) == 7:
				summary1 = temp[5]
			elif len(temp) == 3:
				summary2 = temp[2]
	
	tempMetrics = []
	tempMetrics.append(runName)
	tempMetrics.append(tubeNumber)
	tempMetrics.append(filterStatus)
	tempMetrics.append(panelSize)
	tempMetrics.append(numCells)
	tempMetrics.append(summary1)
	tempMetrics.append(summary2)
	tempMetrics.append(round(panelUniformity,2))
	tempMetrics.append(round(fourtyCov,2))
	tempMetrics.append(round(betweenPoint5And2,2))
	tempMetrics.append(round(betweenPoint2And5,2))
	tempMetrics.append(round(percentReadsToHighFlyers,2))
	tempMetrics.append(round(twentyXamplicons,2))
	tempMetrics.append(round(tenXamplicons,2))
	tempMetrics.append(round(fiveXamplicons,2))
	tempMetrics.append(round(oneXamplicons,2))
	tempMetrics.append(round(totalReads,2))
	tempMetrics.append(round(meanReads * 0.2,2))
	tempMetrics.append(round(cellUniformity1,2))
	tempMetrics.append(round(cellUniformity2,2))
	tempMetrics.append(round(cellUniformity3,2))
	tempMetrics.append(round(cellUniformity4,2))
	tempMetrics.append(numAmpliconsDropped)

	tempMetrics = list(map(str,tempMetrics))
	metrics.append("\t".join(tempMetrics))
	
with open(sys.argv[2],'w') as OUTPUT:
	OUTPUT.write("\n".join(metrics))
	

#!/usr/bin/env bash

echo "amplicon.bed file: $1"
echo "design.bed file: $2"
echo "primer.tab: $3"
echo "design base: $4"
echo "GC cutoff: $5"
echo "reference genome: $6"
#echo "reference genome: $6"


mkdir primerQC

/rhome/shu/src/bedtools2/bin/bedtools getfasta -fi $6 -bed $1 -name -fo primerQC/$4_amplicon.fa
/rhome/shu/src/bedtools2/bin/bedtools getfasta -fi $6 -bed $2 -name -fo primerQC/$4_insert.fa
less $3 | awk '{OFS="\t";print $3,$1,$2}' > $4_primers.tab
less $3 | awk '{print ">"$3"_F\n",$1"\n",">"$3"_R\n",$2}' > $4_primers
sed -e 's/\ //g' $4_primers > $4_primer.fa
mv $4_primer.fa $4_primers.tab primerQC/

cd primerQC
bowtie-build $4_amplicon.fa $4_amplicon.fa

mkdir PrimerSpecificity
mkdir PrimerDimer
mkdir GCcontent
mkdir InsertSpecificity


cd PrimerSpecificity
bowtie -a -v 1 -y -S -f ../$4_amplicon.fa ../$4_primer.fa > primer_refAmpliconfa.bowtie.1mm.out
bowtie -a -v 2 -y -S -f ../$4_amplicon.fa ../$4_primer.fa > primer_refAmpliconfa.bowtie.2mm.out
bowtie -a -v 3 -y -S -f ../$4_amplicon.fa ../$4_primer.fa > primer_refAmpliconfa.bowtie.3mm.out
bowtie -a -v 2 -y -S -f $6 ../$4_primer.fa > primer_ref.bowtie.2mm.out
bowtie -a -v 1 -y -S -f $6 ../$4_primer.fa > primer_ref.bowtie.1mm.out
python ~/src/bowtie_hitscount.py primer_refAmpliconfa.bowtie.2mm.out primer_refAmpliconfa.bowtie.2mm.hitscount
python ~/src/bowtie_hitscount.py primer_ref.bowtie.1mm.out primer_ref.bowtie.2mm.hitscount

cd ../PrimerDimer

python ~/src/PrimerDimer_primer3_v3.py ../$4_primers.tab $4_primerdimer.tab
python ~/src/Select_PrimerDimer_by_dG_reportAmp.py $4_primerdimer.tab -10000 heterodimer_dG fwd_hairpin_dG rev_hairpin_dG fwd_homod_dG rev_homod_dG heterod_amp_dgcutoff_minus9_Primer.tab homod_amp_dgcutoff_minus9.tab heterod_count

cd ../GCcontent
python ~/src/Calc_amp_GC.py ../$4_amplicon.fa ampGC passfilterAmp $5 dropoutAmp

cd ../InsertSpecificity
bowtie -a -v 2 -y -S -f $6 ../$4_insert.fa > insert_refhg19.bowtie.2mm.out
bowtie -a -v 1 -y -S -f $6 ../$4_insert.fa > insert_refhg19.bowtie.1mm.out

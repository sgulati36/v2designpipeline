#!/usr/bin/env python

import sys

############### Inputs
# Argument 1 - Output from checkPrimerFeatures.py
# Argument 2 - Amplicon GC file
# Argument 3 - Primer hits 2mm file from primerQC.sh
# Argument 4 - Normalized mean values appended file
# Argument 5 - Output filename
###############

ampGcFile = sys.argv[2]
calcPropertiesFile = sys.argv[1]
primerHitsFile = sys.argv[3]
mergedValuesFile = sys.argv[4]
outputFile = sys.argv[5]

ampGC = {}
fwdPrimerHits = {}
revPrimerHits = {}
calculatedProperties = {}

with open(ampGcFile) as INPUT:
	for line in INPUT:
		temp = line.strip().split("\t")
		ampGC[temp[0]] = temp[1]

with open(calcPropertiesFile) as INPUT:
	calcPropertiesHeaders = INPUT.readline().strip().split("\t")
	calcPropertiesHeaders = "\t".join(calcPropertiesHeaders[1:])
	for line in INPUT:
		temp = line.strip().split("\t")
		calculatedProperties[temp[0]] = "\t".join(temp[1:])

with open(primerHitsFile) as INPUT:
	header = INPUT.readline().strip()
	for line in INPUT:
		temp1 = line.strip().split("\t")
		temp2 = temp1[0].split("_")
		if temp2[-1] == 'F':
			ampName = "_".join(temp2[:-1])
			fwdPrimerHits[ampName] = "\t".join(temp1[1:])
		elif temp2[-1] == 'R':
			ampName = "_".join(temp2[:-1])
			revPrimerHits[ampName] = "\t".join(temp1[1:])

#print(revPrimerHits.keys())
data = []
finalHeader = "ampliconName\tNormalizedMean\t" + calcPropertiesHeaders + "\t" + "ampliconGC" + "\t" + 'fwd_num_total_hits' + "\t" + 'fwd_num_hits_no_mismatch_in_last10' + "\t" + 'fwd_num_hits_no_mismatch_in_last5' + "\t" + 'rev_num_total_hits' + "\t" + 'rev_num_hits_no_mismatch_in_last10' + "\t" + 'rev_num_hits_no_mismatch_in_last5' 

data.append(finalHeader)

with open(mergedValuesFile) as INPUT:
	for line in INPUT:
		temp = line.strip().split('\t')
		newLine = line.strip() + "\t" + calculatedProperties[temp[0]] + "\t" + ampGC[temp[0]] + "\t" + fwdPrimerHits[temp[0]] + "\t" + revPrimerHits[temp[0]]
		data.append(newLine)

with open(outputFile,'w') as OUTPUT:
	OUTPUT.write('\n'.join(data))

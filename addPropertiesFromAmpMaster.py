#!/usr/bin/env python

import sys

############### Inputs
# Argument 1 - Amp Master file
# Argument 2 - Normalized mean values appended file
# Argument 3 - Corresponding Amp ID file
# Argument 4 - Output filename
###############

ampMasterFile = sys.argv[1]
meanValuesFile = sys.argv[2]
nameMappingFile = sys.argv[3]
outFile = sys.argv[4]

with open(ampMasterFile) as INPUT:
	data = INPUT.read().strip().split("\n")

headers = data[0]
headers = "\t".join(headers.split("\t")[1:])
data = data[1:]
headers = headers.replace('fwd_GC_perc','fwd_GC')
headers = headers.replace('rev_GC_perc','rev_GC')
headers = headers.replace('amplicon_len','amplicon_length')
headers = headers.replace('fwd_length','fwd_len')
headers = headers.replace('rev_length','rev_len')
headers = headers.replace('fwd_primer_maxN','fwd_maxN')
headers = headers.replace('rev_primer_maxN','rev_maxN')
headers = headers.replace('amp_GC','ampliconGC')
properties = {}

for line in data:
	temp = line.strip().split("\t")
	if temp[0] in properties:
		print('repeat amp ID',temp[0])
	else:
		properties[temp[0]] = "\t".join(temp[1:])

mapping = {}

with open(nameMappingFile) as INPUT:
	for line in INPUT:
		temp = line.strip().split("\t")
		mapping[temp[1]] = temp[0]

data = []

with open(meanValuesFile) as INPUT:
	for line in INPUT:
		temp = line.strip().split("\t")
		try:
			data.append(line.strip()+"\t"+properties[temp[0]])
		except:
			data.append(line.strip()+"\t"+properties[mapping[temp[0]]])

headers = "AmpliconID\tNormalizedMean\t" + headers

with open(outFile,"w") as OUTPUT:
	OUTPUT.write(headers+"\n")
	OUTPUT.write("\n".join(data))

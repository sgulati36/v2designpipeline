# V2DesignPipeline

 - All python scripts except checkPrimerFeatures.py use Python3.

 - R scripts have several things hard coded, need to be modified.

Python3 Libraries required:

 - pandas
 - numpy
 - scipy
 - sklearn
 - matplotlib
 - seaborn

Python2 Libraries required:

 - pandas
 - numpy
 - BioPython
 - primer3

R Libraries required:

 - ggpubr
 - gridExtra
 - readr
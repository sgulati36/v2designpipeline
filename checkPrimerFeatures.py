#usr/bin/env python
'''
Author: Shu Wang, Ph.D
Email: wang@missionbio.com
Data: 2018.04.02

input is the primer.tab contains all the primers to be tested.
'''

import primer3
import re, sys, subprocess, os
import pandas as pd
from Bio.SeqUtils import MeltingTemp as mt


cwd=os.getcwd()

def getTm(primerseq):
    tm = mt.Tm_NN(primerseq, Na=0, K=0, Tris=197.5, Mg=5, dnac1=20, dnac2=0, dNTPs=0.4)
    return tm

def calcGC(primerseq):
    seq=primerseq.upper()
    GC = seq.count('G') + seq.count('C')
    gcpercent = 100 * GC / float(len(seq))
    return gcpercent

def max_hpoly(primerseq):
    nhpolys = map(lambda x: map(len, re.findall(x + "{3,}", primerseq)), ["A","T","C","G"])
    if any(nhpolys)==True:
        maxhpoly=max(max(ls) for ls in nhpolys if ls!=[])
    else:
        maxhpoly=0
    return maxhpoly

############Properties of self-dimer (homodimer) and secondary-structure (hairpin) calculated using primer3.
def calhomodimer(primerseq):
    homoinfo=[]
    homores = primer3.calcHomodimer(primerseq, mv_conc=356.12, dv_conc=0.0, dntp_conc=0.4, dna_conc=20.0, temp_c=37, max_loop=30)
    structure_found, tm, dg =homores.structure_found, homores.tm, homores.dg
    homoinfo.extend((structure_found, tm, dg))
    return homoinfo

#run cacluating the haripin structure per primer seq
def calhairpin(primerseq):
    hairpininfo=[]
    hairpinres=primer3.calcHairpin(primerseq, mv_conc=356.12, dv_conc=0.0, dntp_conc=0.4, dna_conc=20.0, temp_c=37, max_loop=30)
    structure_found, tm, dg = hairpinres.structure_found, hairpinres.tm, hairpinres.dg
    hairpininfo.extend((structure_found, tm, dg))
    return hairpininfo

def fwdcountSNP(dbsnp_out):
    countfwdsnp={}
    exactcnt={}
    rangecnt={}
    distance3p={}

    with open(dbsnp_out, 'r') as snpout:
        for l in snpout:
            line=l.strip().split()
            ampid=line[3]

            if ampid not in exactcnt:
                exactcnt[ampid] = 0
            if ampid not in rangecnt:
                rangecnt[ampid] = 0
            if ampid not in distance3p:
                distance3p[ampid]=[]

            if line[8]==".":
                exactcnt[ampid] += 0
                rangecnt[ampid] += 0

            if line[8] == "exact":
                if (int(line[5])+1) <= int(line[2])<=int(line[6]):
                    exactcnt[ampid] += 1
                    dis = 0
                    distance3p.setdefault(ampid, []).append(dis)
                else:
                    exactcnt[ampid] += 1
                    dis = int(line[2]) - int(line[6])
                    distance3p.setdefault(ampid, []).append(dis)

            if line[8] in ["range", "rangeDeletion", "between", "rangeInsertion", "rangeSubstitution"]:  #wht about "between", rangeDel, rangeInsertion etc.?
                if (int(line[5])+1) <= int(line[2])<=int(line[6]):
                    rangecnt[ampid] += 1
                    dis = 0
                    distance3p.setdefault(ampid, []).append(dis)
                else:
                    rangecnt[ampid] += 1
                    dis = int(line[2]) - int(line[6])
                    distance3p.setdefault(ampid, []).append(dis)

    for key in exactcnt.keys():
        countfwdsnp.setdefault('chr_cor', []).append(key)
        countfwdsnp.setdefault('exact_snp', []).append(exactcnt[key])
        countfwdsnp.setdefault('range_snp', []).append(rangecnt[key])
        if distance3p[key]==[]:
            countfwdsnp.setdefault('distance3p', []).append("NULL")
        else:
            countfwdsnp.setdefault('distance3p', []).append(min(distance3p[key]))

    countSNP_df =pd.DataFrame(countfwdsnp)
    return countSNP_df


def revcountSNP(dbsnp_out):
    countrevsnp = {}
    exactcnt = {}
    rangecnt = {}
    distance3p = {}
    with open(dbsnp_out, 'r') as snpout:
        for l in snpout:
            line = l.strip().split()
            ampid = line[3]
            if ampid not in exactcnt:
                exactcnt[ampid] = 0
            if ampid not in rangecnt:
                rangecnt[ampid] = 0
            if ampid not in distance3p:
                distance3p[ampid]=[]

            if line[8] == ".":
                exactcnt[ampid] += 0
                rangecnt[ampid] += 0

            if line[8] == "exact":
                if int(line[5])+1 <= int(line[1])+1 <= int(line[6]):
                    exactcnt[ampid] += 1
                    #dis = int(line[6]) - (int(line[1]) + 1)  # 1-based calculation-- output the closest distance from the rsID to the primer 3prime end
                    dis = 0
                    distance3p.setdefault(ampid, []).append(dis)
                else:
                    exactcnt[ampid] += 1
                    dis = int(line[5]) - int(line[1])
                    distance3p.setdefault(ampid, []).append(dis)

            if line[8] in ["range", "rangeDeletion", "between", "rangeInsertion", "rangeSubstitution"]:  # wht about "between", rangeDel, rangeInsertion etc.?
                if int(line[5])+1 <= int(line[1])+1 <= int(line[6]):
                    rangecnt[ampid] += 1
                    #dis = int(line[6]) - (int(line[1]) + 1)
                    dis = 0
                    distance3p.setdefault(ampid, []).append(dis)
                else:
                    rangecnt[ampid] += 1
                    dis = int(line[5]) - int(line[1])
                    distance3p.setdefault(ampid, []).append(dis)

    for key in exactcnt.keys():
        countrevsnp.setdefault('amp_id', []).append(key)
        countrevsnp.setdefault('exact_snp', []).append(exactcnt[key])
        countrevsnp.setdefault('range_snp', []).append(rangecnt[key])
        if distance3p[key]==[]:
            countrevsnp.setdefault('distance3p', []).append("NULL")
        else:
            countrevsnp.setdefault('distance3p', []).append(min(distance3p[key]))

    countSNP_df = pd.DataFrame(countrevsnp)
    return countSNP_df

def max_hpoly(seq):
    nhpolys = map(lambda x: map(len, re.findall(x + "{9,}", seq)), ["A","T","C","G"])
    if any(nhpolys)==True:
        maxhpoly=max(max(ls) for ls in nhpolys if ls!=[])
    else:
        maxhpoly=0
    return maxhpoly

def main(primerseq, fwdbed, revbed, dbSNP, ampfa, output):
    # here the bed files and dbSNP and output files are the given as the path
    cmd_dbsnp_fwd = "/usr/bin/intersectBed -a " + fwdbed + " -b " + dbSNP + " -loj > " + cwd+"/fwd_dbsnp.out"
    cmd_dbsnp_rev = "/usr/bin/intersectBed -a " + revbed + " -b " + dbSNP + " -loj > " + cwd+"/rev_dbsnp.out"
    subprocess.check_output(cmd_dbsnp_fwd, stderr=subprocess.STDOUT, shell=True)
    subprocess.check_output(cmd_dbsnp_rev, stderr=subprocess.STDOUT, shell=True)

    fwd_SNPcnt_df = fwdcountSNP(cwd+"/fwd_dbsnp.out")
    rev_SNPcnt_df = revcountSNP(cwd+"/rev_dbsnp.out")
    print fwd_SNPcnt_df
    print rev_SNPcnt_df
    fwd_SNPcnt_df.to_csv(cwd+"/fwdsnpout", sep="\t", index=False)
    rev_SNPcnt_df.to_csv(cwd+"/revsnpout", sep="\t", index=False)

    ampSeqs = {}

    with open(ampfa) as INPUT:
        for line in INPUT:
            temp = line.strip().split("\t")
            ampSeqs[temp[0]] = temp[1]

    with open(primerseq, 'r') as pseq, open(output, 'w') as outfile:
        outfile.write("amplicon"+'\t'+"fwd_len"+"\t"+"fwd_Tm"+"\t"+"fwd_GC"+"\t"+"fwd_maxN"+"\t"+"fwd_homoTm"+"\t"+"fwd_homo_dG"+"\t"+"fwd_hairpinTm"+"\t"+"fwd_hairpindG"+"\t"+"rev_len"+"\t"+"rev_Tm"+"\t"+"rev_GC"+"\t"+"rev_maxN"+"\t"+"rev_homoTm"+"\t"+"rev_homo_dG"+"\t"+"rev_hairpinTm"+"\t"+"rev_hairpindG"+"\t"+"amp_MaxHomopolymer"+"\t"+"amplicon_length"+"\n")


        for l in pseq:
            line=l.strip().split()
            amplicon, fwdseq, revseq=line[0], line[1], line[2]
            fwdTm, fwdGC, fwdmaxN =getTm(fwdseq), calcGC(fwdseq), max_hpoly(fwdseq)
            fwdhomoTm, fwdhomodG, fwdhairpinTm, fwdhairpindG=calhomodimer(fwdseq)[1],calhomodimer(fwdseq)[2],calhairpin(fwdseq)[1], calhairpin(fwdseq)[2]
            revTm, revGC, revmaxN=getTm(revseq), calcGC(revseq), max_hpoly(revseq)
            revhomoTm, revhomodG, revhairpinTm, revhairpindG=calhomodimer(revseq)[1],calhomodimer(revseq)[2],calhairpin(revseq)[1], calhairpin(revseq)[2]

            outfile.write(line[0]+'\t'+str(len(line[1]))+"\t"+str(fwdTm)+"\t"+str(fwdGC)+"\t"+str(fwdmaxN)+"\t"+str(fwdhomoTm)+"\t"+str(fwdhomodG)+"\t"+str(fwdhairpinTm)+"\t"+ \
                      str(fwdhairpindG)+"\t"+str(len(line[2]))+"\t"+str(revTm)+"\t"+str(revGC)+"\t"+str(revmaxN)+"\t"+str(revhomoTm)+"\t"+str(revhomodG)+"\t"+str(revhairpinTm)+"\t"+ \
                      str(revhairpindG)+"\t"+str(max_hpoly(ampSeqs[amplicon]))+"\t"+str(len(ampSeqs[amplicon]))+"\n")


if __name__=="__main__":
    f1=sys.argv[1]
    f2 = sys.argv[2]
    f3 = sys.argv[3]
    f4 = sys.argv[4]
    f5 = sys.argv[5]
    o1 = sys.argv[6]

    main(f1, f2, f3, f4, f5, o1)
